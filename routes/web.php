<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

//Route::get('sendbasicemail','MailController@basic_email') ;
Route::get('sendhtmlemail','MailController@html_email') ;
//Route::get('sendattachmentemail','MailController@attachment_email') ;

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

//Rotas para cadastro de unidades
Route::get('unidades/listar',[
	'as'=>'unidades.listar',
	'uses'=>'unidadeController@listar'
]);
Route::get('unidades/form',function(){
	return view('unidades/cadastroUnidade');
});
Route::post('unidades/inserir',[
	'as'=>'unidade.inserir',
	'uses'=>'unidadeController@inserir'
]);
Route::get('unidades/remover/{id}',[
	'as'=>'unidade.remover',
	'uses'=>'unidadeController@remover'
]);
Route::get('unidades/alterar/{id}',[
	'as'=>'unidade.alterar',
	'uses'=>'unidadeController@alterar'
]);
Route::post('unidades/alterar',[
	'as'=>'unidade.alterar',
	'uses'=>'unidadeController@update'
]);

//
//Rotas para cadastro de produtos
Route::get('produtos/listar',[
	'as'=>'produtos.listar',
	'uses'=>'produtoController@listar'
]);
Route::get('produtos/form',[
	'as'=>'produtos.form',
	'uses'=>'produtoController@select'
]);
Route::post('produto/inserir',[
	'as'=>'produto.inserir',
	'uses'=>'produtoController@inserir'
]);
Route::get('produtos/remover/{id}',[
	'as'=>'produto.remover',
	'uses'=>'produtoController@remover'
]);
Route::get('produtos/alterar/{id}',[
	'as'=>'produto.alterar',
	'uses'=>'produtoController@alterar'
]);
Route::post('produtos/alterar',[
	'as'=>'produto.alterar',
	'uses'=>'produtoController@update'
]);
//
//Rotas para cadastro de fornecedores
Route::get('fornecedores/listar',[
	'as'=>'fornecedores.listar',
	'uses'=>'fornecedorController@listar'
]);
Route::get('fornecedores/form',function(){
	return view('fornecedores/cadastroFornecedor');
});
Route::post('fornecedor/inserir',[
	'as'=>'fornecedor.inserir',
	'uses'=>'fornecedorController@inserir'
]);
Route::get('fornecedores/remover/{id}',[
	'as'=>'fornecedor.remover',
	'uses'=>'fornecedorController@remover'
]);
Route::get('fornecedores/alterar/{id}',[
	'as'=>'fornecedor.alterar',
	'uses'=>'fornecedorController@alterar'
]);
Route::post('fornecedores/alterar',[
	'as'=>'fornecedor.alterar',
	'uses'=>'fornecedorController@update'
]);
//
//Rotas para cadastro de orgaos
Route::get('orgaos/listar',[
	'as'=>'orgaos.listar',
	'uses'=>'orgaoController@listar'
]);
Route::get('orgaos/form',function(){
	return view('orgaos/cadastroOrgao');
});
Route::post('orgao/inserir',[
	'as'=>'orgao.inserir',
	'uses'=>'orgaoController@inserir'
]);
Route::get('orgaos/remover/{id}',[
	'as'=>'orgao.remover',
	'uses'=>'orgaoController@remover'
]);
Route::get('orgaos/alterar/{id}',[
	'as'=>'orgao.alterar',
	'uses'=>'orgaoController@alterar'
]);
Route::post('orgao/alterar',[
	'as'=>'orgao.alterar',
	'uses'=>'orgaoController@update'
]);
//
//Rotas para cadastro de locais
Route::get('locais/listar',[
	'as'=>'locais.listar',
	'uses'=>'localController@listar'
]);
Route::get('locais/form',[
	'as'=>'locais.form',
	'uses'=>'localController@select'
]);
Route::post('local/inserir',[
	'as'=>'local.inserir',
	'uses'=>'localController@inserir'
]);
Route::get('locais/remover/{id}',[
	'as'=>'local.remover',
	'uses'=>'localController@remover'
]);
Route::get('locais/alterar/{id}',[
	'as'=>'local.alterar',
	'uses'=>'localController@alterar'
]);
Route::post('locais/alterar',[
	'as'=>'local.alterar',
	'uses'=>'localController@update'
]);
//
//Rotas para cadastro de solicitacoes de materiais
Route::get('solicitacoes/listar',[
	'as'=>'solicitacoes.listar',
	'uses'=>'solicitacaoController@listar'
]);
Route::get('solicitacoes/form',[
	'as'=>'solicitacoes.form',
	'uses'=>'solicitacaoController@select'
]);
Route::post('solicitacao/inserir',[
	'as'=>'solicitacao.inserir',
	'uses'=>'solicitacaoController@inserir'
]);
Route::get('solicitacoes/remover/{id}',[
	'as'=>'solicitacao.remover',
	'uses'=>'solicitacaoController@remover'
]);
Route::get('solicitacoes/detalhes/{id}',[
	'as'=>'solicitacao.detalhes',
	'uses'=>'solicitacaoController@detalhes'
]);
