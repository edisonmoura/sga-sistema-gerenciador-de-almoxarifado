<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CriaTabelaSaidas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('saidas', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_local')->unsigned();
            $table->foreign('id_local')->
                references('id')->
                on('locais')->
                onDelete('cascade');
            $table->integer('id_produto')->unsigned();
            $table->foreign('id_produto')->
                references('id')->
                on('produtos')->
                onDelete('cascade');
            $table->string('quantidade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('saidas');
    }
}
