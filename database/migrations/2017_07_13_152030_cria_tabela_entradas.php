<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CriaTabelaEntradas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('entradas', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_produto')->unsigned();
            $table->foreign('id_produto')->
                references('id')->
                on('produtos')->
                onDelete('cascade');
            $table->integer('id_fornecedor')->unsigned();
            $table->foreign('id_fornecedor')->
                references('id')->
                on('fornecedores')->
                onDelete('cascade');
            $table->integer('id_local')->unsigned();
            $table->foreign('id_local')->
                references('id')->
                on('locais')->
                onDelete('cascade');
            $table->integer('id_nf')->unsigned();
            $table->foreign('id_nf')->
                references('id')->
                on('nfs')->
                onDelete('cascade');
            $table->string('data_validade');
            $table->string('valor_unit');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('entradas');
    }
}
