<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CriaTabelaSituacoes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('situacoes', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_saida')->unsigned();
            $table->foreign('id_saida')->
                references('id')->
                on('saidas')->
                onDelete('cascade');
            $table->integer('id_entrada')->unsigned();    
            $table->foreign('id_entrada')->
                references('id')->
                on('entradas')->
                onDelete('cascade');
            $table->integer('id_produto')->unsigned();    
            $table->foreign('id_produto')->
                references('id')->
                on('produtos')->
                onDelete('cascade');
            $table->integer('id_local')->unsigned();    
            $table->foreign('id_local')->
                references('id')->
                on('locais')->
                onDelete('cascade');
            $table->string('quantidade');            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('situacoes');
    }
}
