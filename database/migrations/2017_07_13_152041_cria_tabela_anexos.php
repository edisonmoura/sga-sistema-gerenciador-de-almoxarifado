<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CriaTabelaAnexos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('anexos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_entrada')->unsigned();
            $table->foreign('id_entrada')->
                references('id')->
                on('entradas')->
                onDelete('cascade');
            $table->string('nome');
            $table->integer('id_nf')->unsigned();
            $table->foreign('id_nf')->
                references('id')->
                on('nfs')->
                onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('anexos');
    }
}
