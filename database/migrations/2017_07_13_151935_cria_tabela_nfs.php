<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CriaTabelaNfs extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('nfs', function (Blueprint $table) {
            $table->increments('id');
            $table->string('data');
            $table->integer('id_fornecedor')->unsigned();
            $table->foreign('id_fornecedor')->
                references('id')->
                on('fornecedores')->
                onDelete('cascade');
            $table->string('numero');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('nfs');
    }
}
