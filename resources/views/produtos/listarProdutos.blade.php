@extends('layouts.app')
@section('content')
<h1>Produtos</h1>
<a href="{{route('produtos.form')}}"><button>Cadastrar</button></a>
<table >
	<tr>
		<td>Nome</td>
		<td>Unidade de medida</td>
		<td>Acao</td>
	</tr>
@foreach($produtos as $produto)
	<tr>
		<td>{{$produto['nome']}}</td>
		<td>{{$produto->unidade['nome']}}</td>
		<td><a href="alterar/{{$produto->id}}"><button>Editar</button></a><a href="remover/{{$produto->id}}"><button>Excluir</button></a></td>
		
	</tr>
	
@endforeach
</table>	
@endsection