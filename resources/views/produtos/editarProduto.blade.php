@extends('layouts.app')
@section('content')
<h1>Edicao de produto</h1>
<form method="POST" action="{{route('produto.alterar')}}">
 {{ csrf_field() }}
	<div class="form-group">
	<label name='nome'>Nome</label>
	<input name="nome" value="{{$produtos['nome']}}">
	<label name='unidade'>Unidade</label>
	<select name="unidade">
		@foreach($unidades as $unidade)
			@if($unidade['id'] == $produtos->unidade['id'])
				<option selected value="{{$unidade['id']}}">{{$unidade['nome']}}</option>
			@else
				<option value="{{$unidade['id']}}">{{$unidade['nome']}}</option>
			@endif
		@endforeach	
	</select>
	</div>
	<input type="hidden" name='id'  value="{{$produtos['id']}}">
	<button type="submit" class="btn btn-danger">Alterar</button>
</form>
@endsection