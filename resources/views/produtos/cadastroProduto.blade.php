@extends('layouts.app')
@section('content')

<h1>Cadastro de Produtos</h1>
<form method="POST" action="{{route('produto.inserir')}}">
 {{ csrf_field() }}
	<div class="form-group">
	<label  name="nome">Nomde do produto</label>
	<input class="form-control" name="nome">
	</div>
	<div class="form-group">
	<label  name="unidade">Unidade de medida</label>
	<select name="unidade">
		@foreach($unidades as $unidade)
			<option value="{{$unidade['id']}}">{{$unidade['nome']}}</option>
		@endforeach	
	</select>
	</div>
	<button type="submit" class="btn btn-danger">Cadastrar</button>
</form>
@endsection