@extends('layouts.app')

@section('content')
@if(Session::has('mensagem_sucesso'))
      {!! 'OK' !!}
      @endif
</br></br>
<div class="container #e0e0e0 grey lighten-2">
    <div class="row">
        <form class="panel-body col s12" method="GET" action="{{url('sendhtmlemail')}}">{{ csrf_field() }}
            <div class="col s5">
                <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                    <div class="input-field">
                      <input id="name" type="text" class="validate" name="name" value="{{ old('name') }}" required autofocus>
                      <label for="name">Nome</label>
                        @if ($errors->has('name'))
                            <span class="help-block">
                                <strong>{{ $errors->first('name') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                    <div class="input-field">
                      <input id="email" type="email" class="validate" name="email" value="{{ old('email') }}" required>
                      <label for="email">E-mail</label>
                        @if ($errors->has('email'))
                            <span class="help-block">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                <div class="form-group">
                    <div class="input-field">
                      <input id="cpf" type="number" class="validate" name="cpf" required>
                      <label for="cpf">Cpf</label>
                    </div>
                </div>
            </div>
            <div class="col s5 col offset-s2">
                <div class="input-field col s12">
                    <select name="local">
                      <option value="" disabled selected>Escolha uma opçao</option>
                      <option value="1">Option 1</option>
                      <option value="2">Option 2</option>
                      <option value="3">Option 3</option>
                    </select>
                    <label>Selecione um Local</label>
                </div>
                <div class="col s12">
                    <button class="btn waves-effect waves-light col offset-s1" type="submit" name="action">Solicitar Cadastro
                        <i class="material-icons right">send</i>
                    </button>
                </div>
            </div>
        </form>
    </div>
</div>
<div id="modal1" class="modal">
    <div class="modal-content">
      <h4>Acesso ao Sistema</h4>
      <p>Ola, o cadastro apenas serve para a solicitaçao de acesso ao sistema. Conforme seus dados sejam validados 
      serao enviados ao administrador, que cedera ou nao a permissao. Obrigado pela compreensao.</p>
      <span>O prazo de resposta se restringe a no maximo 3 horas.</span>
    </div>
    <div class="modal-footer">
      <a href="#!" class="modal-action modal-close waves-effect waves-green btn">OK, ENTENDI</a>
    </div>
  </div>
<footer class="container">
    <div class="row">
        <a class="btn-floating waves-effect waves-light gray tooltipped" data-delay="40" data-tooltip="Duvidas" data-target="modal1">
            <i class="material-icons">info</i>
        </a>
    </div>
</footer>
@endsection
