@extends('layouts.app')

@section('content')
</br></br>
<div class="container #e0e0e0 grey lighten-2">
    <div class="row">
    <form class="panel-body col s6 offset-s3" method="POST" action="{{ route('login') }}">{{ csrf_field() }}
      <div class="row form-group{{ $errors->has('email') ? ' has-error' : '' }}">
        <div class="input-field col s12">
          <input id="email" type="email" class="validate" name="email" value="{{ old('email') }}" required autofocus>
          <label for="email">E-mail</label>
            @if ($errors->has('email'))
                <span class="help-block">
                    <strong>{{ $errors->first('email') }}</strong>
                </span>
            @endif
        </div>
      </div>
      <div class="row form-group{{ $errors->has('password') ? ' has-error' : '' }}">
        <div class="input-field col s12">
          <input id="password" type="password" class="validate" name="password" required>
          <label for="password">Senha</label>
            @if ($errors->has('password'))
                <span class="help-block">
                    <strong>{{ $errors->first('password') }}</strong>
                </span>
            @endif
        </div>
      </div>
      <div class="row form-group{{ $errors->has('password') ? ' has-error' : '' }}">
        <div class="input-field col s12">
          <input type="checkbox" id="remember" name="remember" {{ old('remember') ? 'checked' : '' }}/>
          <label for="remember">Lembre-se de mim</label>
        </div>
       </div>
       <br>
       <div class="row">
            <div class="col s12">
                <button class="btn waves-effect waves-light col offset-s1" type="submit" name="action">Entrar
                    <i class="material-icons right">send</i>
                </button>

                <a class="btn btn-link col offset-s1" href="{{ route('password.request') }}">
                    Esqueceu sua senha?
                </a>
            </div>
        </div>
    </form>
  </div>
</div>
@endsection