<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <title>SGA - Sistema Gerenciador de Almoxarifado</title>

  <!--Import Google Icon Font-->
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
  
  <!-- Compiled and minified CSS -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.99.0/css/materialize.min.css">

  <!-- Compiled and minified JavaScript -->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.99.0/js/materialize.min.js"></script>

  <!--Let browser know website is optimized for mobile-->
  <meta name="viewport" content="width=device-width, initial-scale=1.0"/>  
</head>
<body>
    <nav class="navbar-fixed">
      <div class="nav-wrapper #37474f blue-grey darken-3">
        <a href="#" data-activates="slide-out" id="menu-btn" class="btn-floating btn-large"><i class="material-icons" id="menu-btn-icon">menu</i></a>
        <ul class="right hide-on-med-and-down">
          <li><a href="{{ url('/') }}">Inicio</a></li>
          <li><a href="#">Sobre</a></li>
        </ul>
      </div>
    </nav>
	<ul id="slide-out" class="side-nav">
    <li><div class="user-view">
      <div class="background">
        <img src="{{asset('images/background_login.jpg')}}">
        </div>
        <a href="#"><img class="circle" src="{{asset('images/logo.png')}}"></a>
        <a href="#"><span class="white-text name">John Doe</span></a>
        <a href="#"><span class="white-text email">jdandturk@gmail.com</span></a>
      </div>
    </li>
      @if (Route::has('login'))
          @if (Auth::check())
              <li><a href="{{ url('/home') }}">Inicio</a></li>
          @else
              <li><a href="{{ url('/login') }}">Entrar</a></li>
              <li><a href="{{ url('/register') }}">Cadastre-se</a></li>
          @endif
      @endif
    <li><div class="divider"></div></li>
    <li><a class="subheader">Submenu</a></li>
    <li><a class="waves-effect" href="produtos/listar">Produtos</a></li>
    <li><a class="waves-effect" href="unidades/listar">Unidades de Medida</a></li>
    <li><a class="waves-effect" href="fornecedores/listar">Fornecedores</a></li>
    <li><a class="waves-effect" href="../orgaos/listar">Orgaos</a></li>
    <li><a class="waves-effect" href="../locais/listar">Locais</a></li>
    <li><a class="waves-effect" href="../solicitacoes/listar">Solicitações</a></li>
  </ul>

	<script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
    <script type="text/javascript" src="js/materialize.min.js"></script>
    <script type="text/javascript">
    	$('#menu-btn').sideNav({
      menuWidth: 300, // Default is 300
      edge: 'left', // Choose the horizontal origin
      closeOnClick: true, // Closes side-nav on <a> clicks, useful for Angular/Meteor
      draggable: true, // Choose whether you can drag to open on touch screens,
      onOpen: function(el) { /* Do Stuff */ }, // A function to be called when sideNav is opened
      onClose: function(el) { /* Do Stuff */ }, // A function to be called when sideNav is closed
    }
  );
    </script>
</body>
</html>