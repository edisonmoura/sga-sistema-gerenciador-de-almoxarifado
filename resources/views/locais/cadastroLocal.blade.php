@extends('layouts.app')
@section('content')

<h1>Cadastro de Locais</h1>
<form method="POST" action="{{route('local.inserir')}}">
 {{ csrf_field() }}
	<div class="form-group">
	<label  name="nome">Nomde do local</label>
	<input class="form-control" name="nome">
	</div>
	<div class="form-group">
	<label  name="orgao">Orgao a que pertence</label>
	<select name="orgao">
		@foreach($orgaos as $orgao)
			<option value="{{$orgao['id']}}">{{$orgao['nome']}}</option>
		@endforeach	
	</select>
	</div>
	<button type="submit" class="btn btn-danger">Cadastrar</button>
</form>
@endsection