@extends('layouts.app')
@section('content')
<h1>Locais</h1>
<a href="{{route('locais.form')}}"><button>Cadastrar</button></a>
<table >
	<tr>
		<td>Nome</td>
		<td>Orgao pertencente</td>
		<td>Acao</td>
	</tr>
@foreach($locais as $local)
	<tr>
		<td>{{$local['nome']}}</td>
		<td>{{$local->orgao['nome']}}</td>
		<td><a href="alterar/{{$local->id}}"><button>Editar</button></a><a href="remover/{{$local->id}}"><button>Excluir</button></a></td>
		
	</tr>
	
@endforeach
</table>	
@endsection