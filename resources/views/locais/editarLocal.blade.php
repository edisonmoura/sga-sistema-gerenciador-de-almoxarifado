@extends('layouts.app')
@section('content')

<h1>Editar local</h1>
<form method="POST" action="{{route('local.alterar')}}">
 {{ csrf_field() }}
	<div class="form-group">
	<label  name="nome">Nomde do local</label>
	<input class="form-control" name="nome" value="{{$locais['nome']}}">
	</div>
	<div class="form-group">
	<label  name="orgao">Orgao a que pertence</label>
	<select name="orgao">
		@foreach($orgaos as $orgao)
			@if($orgao['id'] == $locais->orgao['id'])
				<option selected value="{{$orgao['id']}}">{{$orgao['nome']}}</option>
			@else
				<option value="{{$orgao['id']}}">{{$orgao['nome']}}</option>
			@endif
		@endforeach	
	</select>
	</div>
	<input type="hidden" name='id'  value="{{$locais['id']}}">
	<button type="submit" class="btn btn-danger">Cadastrar</button>
</form>
@endsection