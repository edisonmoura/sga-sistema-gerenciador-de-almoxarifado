@extends('layouts.app')
@section('content')
<h1>Edicao de orgao</h1>
<form method="POST" action="{{route('orgao.alterar')}}">
 {{ csrf_field() }}
	<div class="form-group">
	<label>Nome</label>
	<input name="nome" value="{{$orgaos['nome']}}">
	</div>
	<input type="hidden" name='id'  value="{{$orgaos['id']}}">
	<button type="submit" class="btn btn-danger">Alterar</button>
</form>
@endsection