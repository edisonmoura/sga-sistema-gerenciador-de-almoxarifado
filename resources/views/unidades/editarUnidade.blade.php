@extends('layouts.app')
@section('content')
<h1>Edicao de unidade</h1>
<form method="POST" action="{{route('unidade.alterar')}}">
 {{ csrf_field() }}
	<div class="form-group">
	<label>Nome</label>
	<input name="nome" value="{{$unidades['nome']}}">
	</div>
	<input type="hidden" name='id'  value="{{$unidades['id']}}">
	<button type="submit" class="btn btn-danger">Alterar</button>
</form>
@endsection