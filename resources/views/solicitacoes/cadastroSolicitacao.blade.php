@extends('layouts.app')
@section('content')
<h1>Solicitacao de Materiais</h1>
<div class="row">
	<form method="POST" action="{{route('solicitacao.inserir')}}">
		 {{ csrf_field() }}
		<div class="col s5">	
			<label  name="produto">Produto</label>
			<select id="produto" name="produto">
				<option  >Selecione</option>
				@foreach($produtos as $produto)
					<option  value="{{$produto['id']}}">{{$produto['nome']}}</option>
				@endforeach	
			</select>
			<label  name='quant'>Quantidade</label>
			<input id="quant" type="number" name="quant">
		</div>
		<div class="col s1 center"><button id="incluir" type="button"  class="btn-floating btn-medium waves-effect waves-light green"><i class="material-icons">forward</i></button><br>Incluir</div>	
		<div id="lista" class="collection col s5">
			<div class="collection-item col s12"><span class="badge"></span><span class="badge">Quant.</span>Produto</div>
		</div>
		<div class="col s11 center"><button type="submit" onclick="listar()"  class="btn btn-danger">Finalizar</button></div>
		<input type="hidden" id="pedido" name="pedido">
	</form>
	
</div>	
<script type="text/javascript">
	
	$('#incluir').click(function(){
		var pedido=$('#pedido').val()
		var produto=$('.selected').html();
		var quant=$('#quant').val();
		$('#pedido').val(pedido+'Produto: '+produto+'&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp  Quantidade: '+quant+'<br>');
		$('#lista').append('<div class="collection-item col s11"><div onclick="remover_produto(this)" class=" right btn-floating  waves-effect waves-light red"><i class=" material-icons">cancel</i></div><span class="badge">'+quant+'</span>'+produto+'</div>');
	});

	function remover_produto(essa){
		$(essa).parent().remove();
	}
	
</script>
@endsection