@extends('layouts.app')
@section('content')
<h1>Solicitacoes</h1>
<a href="form"><button title="Criar solicitacao" class=" btn-floating  waves-effect waves-light"><i class=" material-icons">add</i></button></a>
<table class="highlight">
	<thead>
		<tr >
			<td>Data</td>
			<td>Solicitante</td>
			<td>Acao</td>
		</tr>
	</thead>
@foreach($solicitacoes as $solicitacao)
	<tbody >
		<tr>
			<td>{{$solicitacao['created_at']}}</td>
			<td >{{$solicitacao['id_solicitante']}}</td>
			<td ><a href="detalhes/{{$solicitacao['id']}}"><button title="Detalhes" class=" btn-floating  waves-effect waves-light"><i class=" material-icons">list</i></button></a><a href="remover/{{$solicitacao['id']}}"><button title="Cancelar" class=" btn-floating  waves-effect waves-light red"><i class=" material-icons">delete</i></button></a></td>
		</tr>
	</tbody>
	
@endforeach
</table>
@endsection