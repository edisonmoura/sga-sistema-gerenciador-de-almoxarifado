@extends('layouts.app')
@section('content')

<h1>Edicao de Fornecedor</h1>
<form method="POST" action="{{route('fornecedor.alterar')}}">
 {{ csrf_field() }}
	<div class="form-group">
	<label  name="nome">Nomde do Fornecedor</label>
	<input class="form-control" name="nome" value="{{$fornecedores->nome}}">
	</div>
	<div class="form-group">
	<label  name="data_ini">Data de inicio do contrato</label>
	<input type="date" class="form-control" name="data_ini" value="{{$fornecedores->data_ini}}">
	<label  name="data_fim">Data de final do contrato</label>
	<input type="date" class="form-control" name="data_fim" value="{{$fornecedores->data_fim}}">
	</div>
	<div class="form-group">
	<label  name="fone">Telefone</label>
	<input type="number" class="form-control" name="fone" value="{{$fornecedores->telefone}}">
	<label  name="email">e-mail</label>
	<input type="mail" class="form-control" name="email" value="{{$fornecedores->email}}">
	<label  name="cnpj">CNPJ</label>
	<input type="number" class="form-control" name="cnpj" value="{{$fornecedores->cnpj}}">
	<label  name="endereco">Endereco</label>
	<input  class="form-control" name="endereco" value="{{$fornecedores->endereco}}">
	</div>
	<input type="hidden" name='id'  value="{{$fornecedores['id']}}">
	<button type="submit" class="btn btn-danger">Cadastrar</button>
</form>
@endsection