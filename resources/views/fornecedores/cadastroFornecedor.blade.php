@extends('layouts.app')
@section('content')

<h1>Cadastro de Fornecedores</h1>
<form method="POST" action="{{route('fornecedor.inserir')}}">
 {{ csrf_field() }}
	<div class="form-group">
	<label  name="nome">Nomde do Fornecedor</label>
	<input class="form-control" name="nome">
	</div>
	<div class="form-group">
	<label  name="data_ini">Data de inicio do contrato</label>
	<input type="date" class="form-control" name="data_ini">
	<label  name="data_fim">Data de final do contrato</label>
	<input type="date" class="form-control" name="data_fim">
	</div>
	<div class="form-group">
	<label  name="fone">Telefone</label>
	<input type="number" class="form-control" name="fone">
	<label  name="email">e-mail</label>
	<input type="mail" class="form-control" name="email">
	<label  name="cnpj">CNPJ</label>
	<input type="number" class="form-control" name="cnpj">
	<label  name="endereco">Endereco</label>
	<input  class="form-control" name="endereco">
	</div>
	<button type="submit" class="btn btn-danger">Cadastrar</button>
</form>
@endsection