<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use App\Fornecedor;


class fornecedorController extends Controller{
	public function inserir(Request $request ){
		$fornecedor=new Fornecedor();
		$fornecedor->nome=$request['nome'];
		$fornecedor->data_ini=$request['data_ini'];
		$fornecedor->data_fim=$request['data_fim'];
		$fornecedor->telefone=$request['fone'];
		$fornecedor->email=$request['email'];
		$fornecedor->cnpj=$request['cnpj'];
		$fornecedor->endereco=$request['endereco'];
		$fornecedor->save();
		return redirect()->to('fornecedores/listar');
	}
	public function listar(){
		$dados['fornecedores']=Fornecedor::all();
		return view('fornecedores/listarFornecedores',$dados);
	}
	public function remover($id){
		$fornecedor=Fornecedor::find($id);
		$fornecedor->delete();
		return redirect()->to('fornecedores/listar'); 
	}
	public function alterar($id){
		$dados['fornecedores']=Fornecedor::find($id);
		return view('fornecedores/editarFornecedor',$dados);
	}
	public function update(Request $request){
		
		$fornecedor=Fornecedor::find($request['id']);
		$fornecedor->nome=$request['nome'];
		$fornecedor->nome=$request['nome'];
		$fornecedor->data_ini=$request['data_ini'];
		$fornecedor->data_fim=$request['data_fim'];
		$fornecedor->telefone=$request['fone'];
		$fornecedor->email=$request['email'];
		$fornecedor->cnpj=$request['cnpj'];
		$fornecedor->endereco=$request['endereco'];
		$fornecedor->save();
		return redirect()->to('fornecedores/listar'); 
	}
	
}