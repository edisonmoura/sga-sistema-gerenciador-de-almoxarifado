<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use App\Local;
use App\Orgao;

class localController extends Controller{
	public function inserir(Request $request ){
		$local=new Local();
		$local->nome=$request['nome'];
		$local->id_orgao=$request['orgao'];
		$local->save();
		return redirect()->to('locais/listar');
	}
	public function listar(){
		$dados['locais']=Local::all();
		//dd($dados['locals'][1]->unidade);
		return view('locais/listarLocais',$dados);
	}
	public function remover($id){
		$local=Local::find($id);
		$local->delete();
		return redirect()->to('locals/listar'); 
	}
	public function alterar($id){
		$dados['locais']=Local::find($id);
		$dados['orgaos']=Orgao::all();
		return view('locais/editarLocal',$dados);
	}
	public function update(Request $request){
		
		$local=Local::find($request['id']);
		$local->nome=$request['nome'];
		$local->id_orgao=$request['orgao'];
		$local->save();
		return redirect()->to('locais/listar'); 
	}
	public function select(){
		$dados['orgaos']=Orgao::all();
		return view('locais/cadastroLocal',$dados);
	}
}