<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use App\Produto;
use App\Unidade;

class produtoController extends Controller{
	public function inserir(Request $request ){
		$produto=new Produto();
		$produto->nome=$request['nome'];
		$produto->id_unidade=$request['unidade'];
		$produto->save();
		return redirect()->to('produtos/listar');
	}
	public function listar(){
		$dados['produtos']=Produto::all();
		//dd($dados['produtos'][1]->unidade);
		return view('produtos/listarProdutos',$dados);
	}
	public function remover($id){
		$produto=Produto::find($id);
		$produto->delete();
		return redirect()->to('produtos/listar'); 
	}
	public function alterar($id){
		$dados['produtos']=Produto::find($id);
		$dados['unidades']=Unidade::all();
		return view('produtos/editarProduto',$dados);
	}
	public function update(Request $request){
		
		$produto=Produto::find($request['id']);
		$produto->nome=$request['nome'];
		$produto->id_unidade=$request['unidade'];
		$produto->save();
		return redirect()->to('produtos/listar'); 
	}
	public function select(){
		$dados['unidades']=Unidade::all();
		return view('produtos/cadastroProduto',$dados);
	}
}