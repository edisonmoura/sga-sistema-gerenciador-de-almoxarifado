<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use App\Orgao;

class orgaoController extends Controller{
	public function inserir(Request $request ){
		$orgao=new Orgao();
		$orgao->nome=$request['nome'];
		$orgao->save();
		return redirect()->to('orgaos/listar');
	}
	public function listar(){
		$dados['orgaos']=Orgao::all();
		return view('orgaos/listarOrgaos',$dados);
	}
	public function remover($id){
		$orgao=Orgao::find($id);
		$orgao->delete();
		return redirect()->to('orgaos/listar'); 
	}
	public function alterar($id){
		$dados['orgaos']=Orgao::find($id);
		return view('orgaos/editarOrgao',$dados);
	}
	public function update(Request $request){
		
		$orgao=orgao::find($request['id']);
		$orgao->nome=$request['nome'];
		$orgao->save();
		return redirect()->to('orgaos/listar');
	}
	
}