<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use App\Solicitacao;
use App\Produto;

class solicitacaoController extends Controller{
	public function inserir(Request $request ){
		$solicitacao= new Solicitacao;
		$solicitacao->id_solicitante=1;
		$solicitacao->pedido=$request['pedido'];
		$solicitacao->status=1;
		$solicitacao->save();
		return redirect()->to('solicitacoes/listar');
	}
	public function listar(){
		$dados['solicitacoes']=Solicitacao::all();
		return view('solicitacoes/listarSolicitacoes',$dados);
	}
	public function remover($id){
		$solicitacao=Solicitacao::find($id);
		$solicitacao->delete();
		return redirect()->to('solicitacoes/listar'); 
	}
	public function detalhes($id){
		$dados['solicitacoes']=Solicitacao::find($id);
		return view('solicitacoes/detalhesSolicitacao',$dados);
	}
	public function select(){
		$dados['produtos']=Produto::all();
		return view('solicitacoes/cadastroSolicitacao',$dados);
	}
}