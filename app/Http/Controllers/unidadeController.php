<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use App\Unidade;

class unidadeController extends Controller{
	public function inserir(Request $request ){
		$unidade=new Unidade();
		$unidade->nome=$request['nome'];
		$unidade->save();
		return redirect()->to('unidades/listar');
	}
	public function listar(){
		$dados['unidades']=Unidade::all();
		return view('unidades/listarUnidades',$dados);
	}
	public function remover($id){
		$unidade=Unidade::find($id);
		$unidade->delete();
		return redirect()->to('unidades/listar'); 
	}
	public function alterar($id){
		$dados['unidades']=Unidade::find($id);
		return view('unidades/editarUnidade',$dados);
	}
	public function update(Request $request){
		
		$unidade=Unidade::find($request['id']);
		$unidade->nome=$request['nome'];
		$unidade->save();
		return redirect()->to('unidades/listar');
	}
	
}