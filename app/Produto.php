<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Produto extends Model
{
    public function unidade() {
        return $this->belongsTo(Unidade::class,'id_unidade');
    }

}
