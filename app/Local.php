<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Local extends Model
{
    protected $table = 'locais';

    public function orgao() {
        return $this->belongsTo(Orgao::class,'id_orgao');
    }

}
